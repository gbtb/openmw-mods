# Copyright Copyright 2019-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil
import sys
from pathlib import Path

from pybuild.info import P

from common.mw import MW, InstallDir


class Package(MW):
    NAME = "Project Atlas"
    DESC = "Texture atlases to improve performance"
    HOMEPAGE = """
        https://github.com/revenorror/Project-Atlas
        https://www.nexusmods.com/morrowind/mods/45399
    """
    LICENSE = "Unlicense"
    RDEPEND = """
        graphic-herbalism? ( gameplay-misc/graphic-herbalism )
        gitd? ( arch-misc/glow-in-the-dahrk )
        !atlasgen? (
            met? ( assets-textures/morrowind-enhanced-textures )
            intelligent-textures? ( assets-textures/intelligent-textures )
        )
    """
    DEPEND = """
        atlasgen? (
            virtual/imagemagick
            modules/configtool
            modules/vfs-rebuild
        )
    """
    KEYWORDS = "openmw"
    # Appears to be mostly identical to the 0.7.3 release on NexusMods, but also includes the atlas generator and a LICENSE file.
    # There was some restructuring that has no effect on the installed result
    # (except for the README being updated),
    # And since we already had a 0.7.2 and 0.7.1, we're going to call this 0.7.3
    SRC_URI = f"""
        https://github.com/MelchiorDahrk/Project-Atlas/releases/download/v0.7.1/Project_Atlas.7z -> {P}.7z
    """
    # TODO: Also support specular and normal maps?
    IUSE = "atlasgen gitd glowing-bitter-coast met intelligent-textures graphic-herbalism smooth"
    DATA_OVERRIDES = """
        assets-misc/morrowind-optimization-patch
        gameplay-misc/graphic-herbalism
    """
    TEXTURE_SIZES = "512 1024"
    TIER = "z"
    PATCHES = "ashtree.patch furn_de.patch limeware.patch"
    S = P

    INSTALL_DIRS = [
        InstallDir("00 Core"),
        InstallDir(
            "01 Textures - Vanilla", REQUIRED_USE="!atlasgen !met !intelligent-textures"
        ),
        InstallDir(
            "01 Textures - MET", REQUIRED_USE="!atlasgen met !intelligent-textures"
        ),
        InstallDir(
            "01 Textures - Intelligent Textures",
            REQUIRED_USE="!atlasgen !met intelligent-textures",
        ),
        InstallDir("02 Urns - Smoothed", REQUIRED_USE="smooth"),
        InstallDir("03 Redware - Smoothed", REQUIRED_USE="smooth"),
        InstallDir("04 Emperor Parasols - Smoothed", REQUIRED_USE="smooth"),
        InstallDir(
            "05 Wood Poles - Hi-Res Texture/textures/ATL",
            RENAME="textures/ATL",
            REQUIRED_USE="texture_size_1024 !atlasgen",
        ),
        InstallDir(
            "05 Wood Poles - Hi-Res Texture",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir("06 Glow in the Dahrk Patch", REQUIRED_USE="gitd"),
        InstallDir(
            "07 Graphic Herbalism Patch",
            REQUIRED_USE="graphic-herbalism",
        ),
        # Improved Lights for All Shaders
        # InstallDir("08 ILFAS Patch"),
        InstallDir(
            "20 BC Mushrooms - Normal - Glowing Bitter Coast Patch",
            REQUIRED_USE="glowing-bitter-coast !smooth",
        ),
        InstallDir(
            "20 BC Mushrooms - Smoothed - Glowing Bitter Coast Patch",
            REQUIRED_USE="glowing-bitter-coast smooth",
        ),
        InstallDir(
            "20 BC Mushrooms - Smoothed", REQUIRED_USE="!glowing-bitter-coast smooth"
        ),
    ]

    def src_prepare(self):
        if "atlasgen" in self.USE:
            os.chdir(self.T)
            args = []
            # TODO: Document on wiki
            if os.getenv("ATLAS_MULTIPLIER"):
                args.extend(["--multiplier", os.getenv("ATLAS_MULTIPLIER")])

            atlas_dir = Path(self.WORKDIR) / self.S / "00 Core/Textures"
            atlases = list((atlas_dir / "atlas").rglob("*.atlas"))
            self.REBUILD_FILES = set()
            # Find textures and copy or link them into this directory
            for atlas in atlases:
                print(f"Generating atlas {atlas}")
                textures = []
                with open(atlas, encoding="utf-8") as file:
                    widths = False
                    for line in file.readlines():
                        if line.startswith("[commands]"):
                            break
                        if widths:
                            texture, _, _ = line.rpartition("=")
                            if texture.strip():
                                textures.append(texture.strip())
                        elif line.startswith("[widths]"):
                            widths = True
                for texture in textures:
                    self.REBUILD_FILES.add(f"textures/{texture}")
                    # delta_plugin extract is much faster, but won't necessarily match what
                    # vfs-rebuild puts in its manifests
                    from configtool.openmw import find_file

                    path = find_file(f"textures/{texture}")
                    try:
                        os.symlink(path, texture)
                    except Exception:
                        shutil.copy(path, texture)
                self.execute(
                    [sys.executable, atlas_dir / "atlasgen.py"] + args + [atlas]
                )
                for texture in textures:
                    os.remove(texture)
            shutil.move("ATL", atlas_dir)

        super().src_prepare()
