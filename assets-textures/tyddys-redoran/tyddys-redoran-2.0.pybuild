# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Redoran - Arkitektora Vol.2"
    DESC = "High resolution textures of Great House Redoran and Ald Skar"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46235"
    KEYWORDS = "openmw"
    LICENSE = "attribution"
    NEXUS_URL = HOMEPAGE
    TEXTURE_SIZES = "1024 2048"
    SRC_URI = """
        texture_size_1024? ( Redoran_Arkitektora_-_MQ-46235-2-0-1545605879.rar )
        texture_size_2048? ( Redoran_Arkitektora_-_HQ-46235-2-0-1545605818.rar )
        ald-skar? (
            texture_size_1024? (
                Redoran_Arkitektora_-_Ald_Skar_HD-46235-1-0-1545606167.rar
            )
            texture_size_2048? (
                Redoran_Arkitektora_-_Ald_Skar_UHD-46235-1-0-1545606051.rar
            )
        )
        atlas? (
            texture_size_1024? (
                Redoran_Arkitektora_-_ATLAS_MQ-46235-1-0-1545606841.rar
            )
            texture_size_2048? (
                Redoran_Arkitektora_-_ATLAS_HQ-46235-1-0-1545606384.rar
            )
        )
        tr? (
            texture_size_1024? (
                Redoran_Arkitektora_for_Tamriel_Rebuild_-_MQ-46235-1-0-1545607089.rar
            )
            texture_size_2048? (
                Redoran_Arkitektora_for_Tamriel_Rebuild_-_HQ-46235-1-0-1545607044.rar
            )
        )
    """
    IUSE = "atlas +ald-skar tr"
    RDEPEND = """
        base/morrowind
        tr? ( landmasses/tamriel-rebuilt )
        atlas? ( assets-misc/project-atlas )
    """
    DATA_OVERRIDES = """
        tr? ( landmasses/tamriel-rebuilt )
        atlas? ( assets-misc/project-atlas )
    """
    INSTALL_DIRS = [
        InstallDir(  # 1024
            ".",
            S="Redoran_Arkitektora_-_MQ-46235-2-0-1545605879",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(  # 2048
            ".",
            S="Redoran_Arkitektora_-_HQ-46235-2-0-1545605818",
            REQUIRED_USE="texture_size_2048",
        ),
        InstallDir(  # 2048
            ".",
            S="Redoran_Arkitektora_-_Ald_Skar_HD-46235-1-0-1545606167",
            REQUIRED_USE="texture_size_1024 ald-skar",
        ),
        InstallDir(  # 4096
            ".",
            S="Redoran_Arkitektora_-_Ald_Skar_UHD-46235-1-0-1545606051",
            REQUIRED_USE="texture_size_2048 ald-skar",
        ),
        InstallDir(  # 2048 (but atlased)
            ".",
            S="Redoran_Arkitektora_-_ATLAS_MQ-46235-1-0-1545606841",
            REQUIRED_USE="texture_size_1024 atlas",
        ),
        InstallDir(  # 4096 (but atlased)
            ".",
            S="Redoran_Arkitektora_-_ATLAS_HQ-46235-1-0-1545606384",
            REQUIRED_USE="texture_size_2048 atlas",
        ),
        InstallDir(  # 1448
            "TR Replacer - Structures - Redoran - копия",
            S="Redoran_Arkitektora_for_Tamriel_Rebuild_-_MQ-46235-1-0-1545607089",
            REQUIRED_USE="texture_size_1024 tr",
        ),
        InstallDir(  # 2896
            "TR Replacer - Structures - Redoran",
            S="Redoran_Arkitektora_for_Tamriel_Rebuild_-_HQ-46235-1-0-1545607044",
            REQUIRED_USE="texture_size_2048 tr",
        ),
    ]
