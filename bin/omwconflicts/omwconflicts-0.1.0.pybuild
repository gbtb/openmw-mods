# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import stat

from pybuild.info import PV, P

from common.exec import Exec


class Package(Exec):
    NAME = "OMWConflicts"
    DESC = "A Terminal User Interface for viewing conflicting files in the OpenMW VFS"
    HOMEPAGE = "https://gitlab.com/portmod/omwconflicts"
    DOWNLOAD_ROOT = (
        "https://gitlab.com/api/v4/projects/33993500/packages/generic/omwconflicts"
    )
    SRC_URI = f"""
        platform_linux? ( {DOWNLOAD_ROOT}/{PV}/{P}-linux-amd64.zip )
        platform_win32? ( {DOWNLOAD_ROOT}/{PV}/{P}-windows-amd64.zip )
        platform_darwin? ( {DOWNLOAD_ROOT}/{PV}/{P}-darwin-amd64.zip )
    """
    LICENSE = "GPL-3"
    KEYWORDS = "openmw"
    IUSE = "platform_linux platform_win32 platform_darwin"

    def src_prepare(self):
        super().src_prepare()
        os.makedirs("bin")
        exe_name = (
            "omwconflicts.exe" if "platform_win32" in self.USE else "omwconflicts"
        )
        # Add executable permissions in case they weren't included
        # when the file was extracted for some reason
        os.chmod(
            exe_name,
            os.stat(exe_name).st_mode | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH,
        )
        path = os.path.join(self.WORKDIR, self.S, "bin", exe_name)
        os.rename(exe_name, path)
